#!/bin/bash

# Print the git commit associated with a version.

set -e
set -u

if [ $# -ne 1 ]; then
    echo >&2 "Usage: $0 UPSTREAM_VERSION"
    exit 1
fi

version=$1

# Get the "real" upstream version
version=$( debian/helpers/real-upstream-version.sh "$version" )

# Get corresponding git commit
awk -F ': ' '$1 == "'"$version"'" {print $2}' debian/helpers/gitcommits
