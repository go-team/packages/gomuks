gomuks (0.3.1+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: repackaged, dropping deb/ and debian/.
  * d/patches/series: drop 0001-change-debug-logging.patch.

 -- Alex Myczko <tar@debian.org>  Tue, 16 Jul 2024 15:24:08 +0000

gomuks (0.3.0-3) unstable; urgency=medium

  * De-vendor all libs, will use debian-packaged ones
  * Remove references to vendor dir in d/rules and d/clean
  * Remove copyright notices of de-vendored libs
  * Adjust new builddeps
  * Add patch to use codenine cbind
  * Add patch to use gdamore tcell
  * Change Uploader Gurkan => Alex Myczko <tar@debian.org>

 -- Nilesh Patra <nilesh@debian.org>  Wed, 10 Apr 2024 18:59:34 +0530

gomuks (0.3.0-2) unstable; urgency=medium

  * d/p/debian/patches/0001-change-debug-logging.patch:
    + Add patch to change default logging behavior on gomuks. It is
      now set to XDG_STATE_HOME as per freedesktop recommendations and a
      fallback to ~/.local/state. Thi can be changed with DEBUG_DIR env var
    + In addition, it'd log only if DEBUG is set to true
  * d/NEWS: Add a NEWS item to highlight about the change in debug logging
  * d/control:
    + Change section to net
    + Add a recommends on ffmpeg. This is needed for including video/audio
      file metadata while issuing a /upload command
    + Add Gürkan Myczko to uploaders
    + Enable Built-Using field (as per go-team way)
  * d/helpers:
    + Add helper scripts to extract out commit hash for a particular tag
      (This has been sourced from docker.io's d/helpers with some
       modifications)
  * d/rules:
    + Propagate ldflags to set tag, builddate and commithash so as
      to set version properly in resulting gomuks binary

 -- Nilesh Patra <nilesh@debian.org>  Thu, 13 Jul 2023 08:10:40 +0530

gomuks (0.3.0-1) unstable; urgency=medium

  * Initial release (Closes: #1039954)

 -- Nilesh Patra <nilesh@debian.org>  Fri, 30 Jun 2023 07:16:44 +0530
